$c->{plugins}{"Storage::LocalRD"}{params}{disable} = 0;

# Alias the LocaldRD plugin to the default local storage plugin
$c->{plugin_alias_map}->{"Storage::Local"} = "Storage::LocalRD";
$c->{plugin_alias_map}->{"Storage::LocalRD"} = undef;

# Location of storage mount point. Ensure this is writable by the relevant
# users e.g. eprints. 
$c->{plugins}->{"Storage::Local"}->{params}->{mount_path} = "/rds";
